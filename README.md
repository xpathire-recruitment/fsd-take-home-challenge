# FSD Take home challenge



## The Challenge
Create a `Django` project to manage a newsfeed (activity stream) similar to `Facebook` / `LinkedIn` newsfeed.

### Details
- The application should be able to create, delete, edit and list the newsfeed items for a user.
- Use the `DRF - Django Rest Framework` to create and expose API endpoints for the CRUD actions mentioned above.
- For the sake of simplicity, we will display the posts that were created by the current user in his/her newsfeed. There are no friends in the system.
- There is no need to implement a reaction system (Likes, comments, sharing, etc. This is out of scope)
- Create a bare minimum single page frontend to allow the user to create, edit, delete and list the newsfeed items.
- Use a frontend framework of your choice (React / Vue.js) to access he API endpoints.
- The API authentication and authorization is out of scope. You are not required to implement this.
- Create a `README.md` file and explain the project and the steps to execute the code.
- Extra points for clean code, code formatting / linting and tests.
- Optional - You can dockerize the application. Extra points for this step.
- Optional - Generate a swagger Open API documentation. Extra points of this step

### How to submit?
Once you are done with the code challenge, please zip your project directory and upload it to Google Drive and share the google drive folder with us (use `share with everyone with a link` option).
You can then share this link with us via email.
